package com.example;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;

/**
 * Created by guido on 23/11/16.
 */

/**
 * This class kicks off the whole process
 *
 * TODO: has too many System.out.println() that should be removed to be testable, most likely with
 * TODO:  callbacks from the calling class that should implement the console interaction (MVP style)
 */
public class FileProcessor {
    private static final String PRIME_NUMBER_TAG = " *** PRIME NUMBER";

    private Dictionary mDictionary = new Dictionary();

    public void readFile(String filename) {
        // TODO something more sophisticated could be used here to improve read performance
        Scanner sc = null;
        try {
            sc = new Scanner(new File(filename));
            while (sc.hasNext()) {
                String s = sc.next();
                addRawWordToDictionary(s);
            }

            printDictionary(mDictionary.getConsoleSorted());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("File not found");
        }
    }

    /**
     * For some reason the source text seems to contain characters "--" that replace a word or a space
     * If that happens words are split in two (or more) words and added separately
     *
     * @param word
     */
    private void addRawWordToDictionary(String word) {
        if (word.contains("--")) {
            String[] parts = word.split("--");

            for (String part : parts) {
                addSingleWord(part);
            }

        } else {
            addSingleWord(word);
        }
    }

    /**
     * removes all chars that are not letters, numbers, dashes, and apostrophes
     * then convert to lower case and finally add them to the dictionary
     *
     * @param word
     */
    private void addSingleWord(String word) {
        String uniqueWord = word.replaceAll("[^a-zA-Z0-9-']", "").toLowerCase();

        mDictionary.putWord(uniqueWord);
    }

    /**
     * after creating the dictionary we can now print the result
     */
    private void printDictionary(Dictionary dictionary) {

        for (Map.Entry<String, Integer> entry: dictionary.entrySet() ) {
            int value = (Integer) entry.getValue();
            System.out.println(entry.getKey() + " = " + value +
                    (MathUtils.isPrime(value) ? PRIME_NUMBER_TAG : ""));
        }
        
        System.out.println("Total entries = " + dictionary.size());
    }
}
