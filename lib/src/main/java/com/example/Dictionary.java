package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by guido on 23/11/16.
 *
 * Implements some basic logic for adding new words and sort the list
 */
public class Dictionary extends LinkedHashMap<String, Integer> {

    public Integer putWord(String key) {
        Integer value = get(key);

        // if value is null, sets the new value to 1 otherwise it increases it

        // Java 8 syntax
        // this syntax would fail unit test but not in real world, I suspect becasue of the way I return the value
        // return put(key, getOrDefault(key,0)+1);

        // this syntax would fail unit test but not in real world, I suspect becasue of the way I return the value
        // return put(key, (value == null) ? 1 : value + 1);

        // this syntax seems to work
        if (value==null){
            super.put(key,1);
            return 1;
        } else {
            super.put(key,value+1);
            return value+1;
        }
    }

    /**
     * Sort in some console useful way, f.e. value asc, key desc
     * Given that the output is on console, it'll be more readable for the user
     *
      */
    public Dictionary getConsoleSorted() {

        List<String> mapKeys = new ArrayList<>(keySet());
        List<Integer> mapValues = new ArrayList<>(values());
        Collections.sort(mapValues);
        Collections.sort(mapKeys);


        Dictionary sortedMap =
                new Dictionary();

        Iterator<Integer> valueIt = mapValues.iterator();
        while (valueIt.hasNext()) {
            Integer val = valueIt.next();
            Iterator<String> keyIt = mapKeys.iterator();

            while (keyIt.hasNext()) {
                String key = keyIt.next();
                Integer comp1 = get(key);
                Integer comp2 = val;

                if (comp1.equals(comp2)) {
                    keyIt.remove();
                    sortedMap.put(key, val);
                    break;
                }
            }
        }
        return sortedMap;
    }
}
