package com.example;

/**
 * Created by guido on 23/11/16.
 */

public class MathUtils {
    /**
     * Simple function that calculates if a number is prime
     * TODO having to repeat this operation often it would make sense to cache prime numbers for avoiding recalculating them
     *
     * @param n number to evaluate
     * @return whether it is or isn't a prime number
     */
    public static boolean isPrime(long n) {
        // fast even test.
        if (n > 2 && (n & 1) == 0)
            return false;

        // only odd factors need to be tested up to n^0.5
        for (int i = 3; i * i <= n; i += 2)
            if (n % i == 0)
                return false;

        return true;
    }

}
