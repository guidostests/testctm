package com.example;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by guido on 23/11/16.
 */
public class DictionaryTest {
    Dictionary mDictionary;

    @Before
    public void setUp() throws Exception {
        mDictionary = new Dictionary();
    }

    @After
    public void tearDown() throws Exception {
        mDictionary = null;
    }

    @Test
    public void putWord() throws Exception {
        assertEquals(1, (int) mDictionary.putWord("first"));
        assertEquals(2, (int) mDictionary.putWord("first"));
        assertEquals(1, (int) mDictionary.putWord("second"));
        assertEquals(3, (int) mDictionary.putWord("first"));
    }

    @Test
    public void getConsoleSorted() throws Exception {
        mDictionary.putWord("first");
        mDictionary.putWord("first");
        mDictionary.putWord("second");
        mDictionary.putWord("first");
        mDictionary.putWord("second");

        // not alpha sorted
        mDictionary.putWord("alpha");
        mDictionary.putWord("charlie");
        mDictionary.putWord("beta");

        Dictionary sortedDictionary = mDictionary.getConsoleSorted();

        // cannot easily unit test a LinkedHashMap, but I can test keySet() and values() with hamcrest matchers
        List<String> mapKeys = new ArrayList<>(sortedDictionary.keySet());
        List<Integer> mapValues = new ArrayList<>(sortedDictionary.values());

        // check that the size is correct
        assertThat(mapKeys, hasSize(5));
        assertThat(mapValues, hasSize(5));

        // check the correct ordering
        assertThat(mapKeys, contains("alpha", "beta", "charlie", "second", "first"));
        assertThat(mapValues, contains(1, 1, 1, 2, 3));
    }


}