package com.example;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by guido on 23/11/16.
 */
public class MathUtilsTest {
    @Test
    public void isPrime() throws Exception {
        assertEquals(true, MathUtils.isPrime(1));
        assertEquals(true, MathUtils.isPrime(2));
        assertEquals(true, MathUtils.isPrime(3));
        assertEquals(false, MathUtils.isPrime(4));
        assertEquals(true, MathUtils.isPrime(5));

        assertEquals(false, MathUtils.isPrime(10));

        assertEquals(true, MathUtils.isPrime(13));
        assertEquals(true, MathUtils.isPrime(19));

        assertEquals(true, MathUtils.isPrime(199));
        assertEquals(false, MathUtils.isPrime(200));
    }

}